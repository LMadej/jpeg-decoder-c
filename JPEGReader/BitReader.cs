﻿using System;
using System.IO;

namespace JPEGReader
{
    /// <summary>
    /// Klasa wspomagająca odczyt bajtów z opracowywanego pliku.
    /// </summary>
    public class BitReader : IDisposable
    {
        private int _cnt = 0;
        private uint _buf = 0;
        private Stream _stream;

        public BitReader(Stream stream) { _stream = stream; }

        private void EnsureData(int bitCounter)
        {
            int todo = ((bitCounter - _cnt) + 7) / 8;
            for (int i = 0; i < todo; i++)
                _buf = (_buf << 8) | (uint)_stream.ReadByte();
            _cnt += (todo > 0) ? todo * 8 : 0;
        }

        public int Peek(int bitCounter)
        {
            EnsureData(bitCounter);
            int mask = ((1 << _cnt) - 1) ^ ((1 << (_cnt - bitCounter)) - 1);
            return (int)(_buf & mask) >> (_cnt - bitCounter);
        }

        public int Read(int bitCounter)
        {
            EnsureData(bitCounter);
            int mask = ((1 << _cnt) - 1) ^ ((1 << (_cnt - bitCounter)) - 1);
            int val = (int)(_buf & mask) >> (_cnt - bitCounter);
            _cnt -= bitCounter;
            return val;
        }

        public void Skip(int bitCounter)
        {
            EnsureData(bitCounter);
            _cnt -= bitCounter;
        }
        public void Dispose()
        {
            if (_stream == null)
                return;

            _stream.Dispose();
        }

    }

}
