﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;

namespace JPEGReader
{
    /// <summary>
    /// Główna klasa programu wywołująca pozostałe. 
    /// Zapisuje plik wynikowy dodając do nazwy "converted"
    /// </summary>
    class JPEGReader
    {
        public string resultfile;

        public string MakeBMP(string filename)
        {
            var jpeg = new Jpeg(filename);
            byte[] img = jpeg.DecodeScan();
            var bmp = new Bitmap((jpeg.Width + 15) & ~0xf, (jpeg.Height + 15) & ~0xf, PixelFormat.Format24bppRgb);
            var dat = bmp.LockBits(new Rectangle(Point.Empty, bmp.Size), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            Marshal.Copy(img, 0, dat.Scan0, img.Length);
            bmp.UnlockBits(dat);
            resultfile = Path.GetFileNameWithoutExtension(filename) + "converted.bmp";
            bmp.Save(resultfile, ImageFormat.Bmp);
            return "Width: " + jpeg.Width + " Height: " + jpeg.Height + "\nJPEG decoding done. BMP file saved as: " + resultfile;
        }
    }
}
