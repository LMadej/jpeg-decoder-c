﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace JPEGReader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void selectJPEGFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "JPEG Files |*.jpg; *.jpeg|All Files |*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            richTextBox1.Clear();
            JPEGReader jpegReader = new JPEGReader();
            richTextBox1.Text = jpegReader.MakeBMP(openFileDialog1.FileName);
            Image image = Image.FromFile(openFileDialog1.FileName);
            pictureBox1.Image = image;
            this.pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            Image image2 = Image.FromFile(jpegReader.resultfile);
            pictureBox2.Image = image2;
            this.pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
        }
        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
