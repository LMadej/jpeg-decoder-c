﻿using System;
using System.Collections.Generic;
using System.IO;


namespace JPEGReader
{
    /// <summary>
    /// Klasa reprezentująca dane otrzymane z pliku JPEG.
    /// </summary>
    public class Jpeg
    {
        private int xres, yres;
        private byte[][] _dqt = new byte[4][];
        private Huffman[][] _dht = new Huffman[4][];
        private byte[] _sdata;
        const int BPP = 3;
        private struct Huffman
        {
            public int b, v;
        }

        public int Width
        {
            get { return xres; }
        }

        public int Height
        {
            get { return yres; }
        }

        public Jpeg(string filen)
        {
            using (var s = File.OpenRead(filen))
            using (var r = new BinaryReader(s))
                ParserF(r);
        }

        private void ParserF(BinaryReader r)
        {
            while (r.PeekChar() >= 0)
            {
                if (r.ReadByte() != 0xFF) break;

                switch (r.ReadByte())
                {

                    case 0xC0: ParserSof0(r); break;
                    case 0xC4: ParserDht(r); break;
                    case 0xD8: break;
                    case 0xD9: return;
                    case 0xDA: ParserSos(r); break;
                    case 0xDB: ParserDqt(r); break;
                    case 0xE0: ParserApp(r, 0); break;
                    case 0xE1: ParserApp(r, 1); break;
                    case 0xE2: ParserApp(r, 2); break;
                    case 0xE3: ParserApp(r, 3); break;
                    case 0xE4: ParserApp(r, 4); break;
                    case 0xE5: ParserApp(r, 5); break;
                    case 0xE6: ParserApp(r, 6); break;
                    case 0xE7: ParserApp(r, 7); break;
                    case 0xE8: ParserApp(r, 8); break;
                    case 0xE9: ParserApp(r, 9); break;
                    case 0xEA: ParserApp(r, 10); break;
                    case 0xEB: ParserApp(r, 11); break;
                    case 0xEC: ParserApp(r, 12); break;
                    case 0xED: ParserApp(r, 13); break;
                    case 0xEE: ParserApp(r, 14); break;
                    case 0xEF: ParserApp(r, 15); break;

                    default: throw new Exception("Detected marker unknown");
                }
            }
        }

        private void ParserSof0(BinaryReader r)
        {
            int length = r.ReadInt();
            if (r.ReadByte() != 8) throw new NotImplementedException();
            yres = r.ReadInt();
            xres = r.ReadInt();
            if (r.ReadByte() != 3) throw new NotImplementedException();

            for (int i = 0; i < 3; i++)
            {
                r.ReadByte();
                int samp = r.ReadByte();
                r.ReadByte();
            }
        }

        private readonly byte[] NatOrder = new byte[] 
        {
			 0,  1,  8, 16,  9,  2,  3, 10,
			17, 24, 32, 25, 18, 11,  4,  5,
			12, 19, 26, 33, 40, 48, 41, 34,
			27, 20, 13,  6,  7, 14, 21, 28,
			35, 42, 49, 56, 57, 50, 43, 36,
			29, 22, 15, 23, 30, 37, 44, 51,
			58, 59, 52, 45, 38, 31, 39, 46,
			53, 60, 61, 54, 47, 55, 62, 63,
		};

        private void ParserDqt(BinaryReader r)
        {
            r.BaseStream.Seek(2, SeekOrigin.Current);
            byte id = r.ReadByte();

            if ((id & 0xF0) >> 4 != 0)
                throw new NotImplementedException();

            if (_dqt[id] == null)
                _dqt[id] = new byte[64];

            for (int i = 0; i < 64; i++)
                _dqt[id][NatOrder[i]] = r.ReadByte();
        }




        private void ParserDht(BinaryReader r)
        {
            int length = r.ReadInt();
            int select = r.ReadByte();
            select = ((select & 0xf0) >> 3) | (select & 0x0f);

            if (_dht[select] == null)
                _dht[select] = new Huffman[0x10000];

            byte[] lens = r.ReadBytes(16);

            int c = 0;
            for (int i = 1; i <= 16; i++)
            {
                for (int j = 0; j < lens[i - 1]; j++)
                {
                    byte val = r.ReadByte();

                    int x = 16 - i;
                    int lo = c << x;
                    int hi = c << x | ((1 << x) - 1);
                    for (int k = lo; k <= hi; k++)
                        _dht[select][k] = new Huffman() { b = i, v = val };

                    c += 1;
                }
                c <<= 1;
            }
        }


        private void ParserSos(BinaryReader r)
        {
            int length = r.ReadInt();
            if (r.ReadByte() != 3) throw new NotImplementedException();

            for (int i = 0; i < 3; i++)
            {
                r.ReadByte();
                int samp = r.ReadByte();
                if (i == 0 && samp != 0x00) throw new NotImplementedException();
                if (i != 0 && samp != 0x11) throw new NotImplementedException();
            }

            if (r.ReadByte() != 0) throw new NotImplementedException();
            if (r.ReadByte() != 63) throw new NotImplementedException();
            r.ReadByte();

            ReadScanData(r);
        }
        private void ParserApp(BinaryReader r, int app)
        {
            int length = r.ReadInt();
            r.BaseStream.Seek(length - 2, SeekOrigin.Current);
        }

        private void ReadScanData(BinaryReader r)
        {
            List<byte> sdata = new List<byte>(1024);
            while (true)
            {
                byte b = r.ReadByte();

                if (b == 0xFF)
                {
                    if (r.ReadByte() == 0x00)
                        sdata.Add(0xFF);
                    else
                    {
                        r.BaseStream.Seek(-2, SeekOrigin.Current);
                        break;
                    }
                }
                else sdata.Add(b);
            }

            sdata.Add(0);
            sdata.Add(0);

            _sdata = sdata.ToArray();
        }

        public byte Clamp(int n)
        {
            if (n < 0x00) return 0x00;
            if (n > 0xFF) return 0xFF;
            return (byte)n;
        }


        public void YCbCrToRgb(int[][] src, byte[] dst, int mx, int my, int stride)
        {
            int blk = (my * 16) * stride + (mx * 16 * BPP);
            for (int bl = 0; bl < 4; bl++)
            {
                int ro = (bl & 2) != 0 ? 8 : 0;
                int co = (bl & 1) != 0 ? 8 : 0;

                for (int y = 0, yy = 0, cy = 0; y < 8; y++, yy += 8, cy += 4)
                {
                    int off = blk + ((ro + y) * stride) + (co * BPP);
                    for (int x = 0; x < 8; x++)
                    {
                        int ty = src[bl][yy + x] + 128;
                        int tcb = src[5][cy + (x / 2)];
                        int tcr = src[4][cy + (x / 2)];
                        int r = (int)Math.Round(ty + (1.402 * tcr));
                        int g = (int)Math.Round(ty - (0.344 * tcb) - (0.714 * tcr));
                        int b = (int)Math.Round(ty + (1.772 * tcb));
                        dst[off++] = Clamp(r);
                        dst[off++] = Clamp(g);
                        dst[off++] = Clamp(b);
                    }
                }
            }
        }

        public byte[] DecodeScan()
        {
            int xMCU = (xres + 15) / 16;
            int yMCU = (yres + 15) / 16;
            int mcu = xMCU * yMCU;

            int stride = ((xMCU * 16 * BPP) + 3) & ~0x03;
            byte[] image = new byte[yMCU * 16 * stride];
            using (MemoryStream ms = new MemoryStream(_sdata))
            {
                BitReader r = new BitReader(ms);

                int dY = 0, dCb = 0, dCr = 0;
                int[][] block = new int[6][];
                for (int y = 0; y < yMCU; y++)
                    for (int x = 0; x < xMCU; x++)
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            block[i] = DecodingBlock(r, false);
                            block[i][0] += dY; dY = block[i][0];
                            DequantizationBlock(block[i], false);
                        }

                        block[4] = DecodingBlock(r, true);
                        block[4][0] += dCb; dCb = block[4][0];
                        DequantizationBlock(block[4], true);

                        block[5] = DecodingBlock(r, true);
                        block[5][0] += dCr; dCr = block[5][0];
                        DequantizationBlock(block[5], true);

                        for (int i = 0; i < 6; i++)
                            block[i] = DCT.DoIDCT(block[i]);

                        YCbCrToRgb(block, image, x, y, stride);
                    }
            }

            return image;
        }

        private void DequantizationBlock(int[] block, bool chrom)
        {
            byte[] dqt = _dqt[chrom ? 1 : 0];

            for (int i = 0; i < 8 * 8; i++)
                block[i] *= dqt[i];
        }

        private static int DecodingNumber(int num, int bits)
        {
            return num < (1 << (bits - 1)) ? num - ((1 << bits) - 1) : num;
        }

        private int[] DecodingBlock(BitReader r, bool chrom)
        {
            Huffman h;
            int tab = chrom ? 1 : 0;

            var result = new int[64];

            h = _dht[0 + tab][r.Peek(16)];
            r.Skip(h.b);
            result[0] = DecodingNumber(r.Read(h.v), h.v);


            for (int i = 1; i < 64; i++)
            {
                h = _dht[2 + tab][r.Peek(16)];
                r.Skip(h.b);

                switch (h.v)
                {
                    case 0x00: i = 63; break;
                    case 0xf0: i += 16; continue;
                    default:
                        i += (h.v & 0xf0) >> 4;
                        if (i >= NatOrder.Length) i = i % 64;
                        result[NatOrder[i]] = DecodingNumber(r.Read(h.v & 0x0f), h.v & 0x0f);
                        break;
                }
            }
            return result;
        }


    }
}
